package com.test.apply.entity;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Hits {

	@JsonProperty("created_at")
	private String createdAt;

	@JsonProperty("title")
	private String title;

	@JsonProperty("url")
	private String url;

	@JsonProperty("author")
	private String author;

	@JsonProperty("points")
	private Integer points;

	@JsonProperty("story_text")
	private String storyText;

	@JsonProperty("comment_text")
	private String commentText;

	@JsonProperty("num_comments")
	private Integer numComments;

	@JsonProperty("story_id")
	private Integer storyId;

	@JsonProperty("story_title")
	private String storyTitle;

	@JsonProperty("story_url")
	private String storyUrl;

	@JsonProperty("parent_id")
	private Integer parentId;

	@JsonProperty("created_at_i")
	private Integer createdAtI;

	@JsonProperty("_tags")
	private String[] tags;

	@JsonProperty("objectID")
	private String objectId;

	@JsonProperty("_highlightResult")
	private HighlightResult highlightResult;

}
