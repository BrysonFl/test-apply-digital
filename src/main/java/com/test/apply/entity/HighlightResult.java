package com.test.apply.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class HighlightResult {

	@JsonProperty("author")
	private General author;

	@JsonProperty("comment_text")
	private CommentText commentText;

	@JsonProperty("story_title")
	private General storyTitle;

	@JsonProperty("story_url")
	private General storyUrl;

}
