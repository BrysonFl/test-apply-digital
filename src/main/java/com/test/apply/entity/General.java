package com.test.apply.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class General {

	@JsonProperty("value")
	private String value;
	private String matchLevel;
	private String[] matchedWords;

}
