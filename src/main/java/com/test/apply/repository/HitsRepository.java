package com.test.apply.repository;

import com.test.apply.model.Hits;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface HitsRepository extends MongoRepository<Hits, String> {
}
