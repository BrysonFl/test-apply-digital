package com.test.apply.entity;

import lombok.Data;

@Data
public class Exhaustive {

	private Boolean nbHits;
	private Boolean typo;
	
}
