package com.test.apply.model;

//import com.test.dto.HighlightResult;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.List;

@Data
@Document
public class Hits {

    @Id
    private String id;

    private LocalDate createdAt;
    private String title;
    private String url;
    private String author;
    private Integer points;
    private String storyText;
    private String commentText;
    private Integer numComments;
    private Integer storyId;
    private String storyTitle;
    private String storyUrl;
    private Integer parentId;
    private Integer createdAtI;
    private List<String> tags;
    private String objectId;
    //private HighlightResult highlightResult;

}
