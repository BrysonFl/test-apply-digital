package com.test.apply.tasks;

import com.test.apply.model.Hits;
import com.test.apply.repository.HitsRepository;
import com.test.apply.response.ResponseServiceAlgolia;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class Task {

	//@Scheduled(initialDelay = 3000, fixedDelay = 3000)
	public ResponseServiceAlgolia getDataApiHour() {
		RestTemplate restTemplate = new RestTemplate();
		
		return restTemplate.getForObject("https://hn.algolia.com/api/v1/search_by_date?query=java", ResponseServiceAlgolia.class);
	}

}
