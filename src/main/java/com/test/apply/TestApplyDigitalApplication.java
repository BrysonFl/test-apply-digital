package com.test.apply;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestApplyDigitalApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestApplyDigitalApplication.class, args);
	}

}
