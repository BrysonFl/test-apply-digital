package com.test.apply.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.test.apply.entity.Exhaustive;
import com.test.apply.entity.Fetch;
import com.test.apply.entity.Hits;
import com.test.apply.entity.Request;

import lombok.Data;

@Data
public class ResponseServiceAlgolia {

	private Hits[] hits;
	private Integer nbHits;
	private Integer page;
	private Integer nbPages;
	private Integer hitsPerPage;
	private Boolean exhaustiveNbHits;
	private Boolean exhaustiveTypo;
	private Exhaustive exhaustive;
	private String query;
	private String params;
	private Integer processingTimeMS;
	private Fetch fetch;
	private Request request;
	private Integer serverTimeMS;
	
}
