package com.test.apply.entity;

import java.util.List;

import lombok.Data;

@Data
public class CommentText {
	
	private String value;
	private String matchLevel;
	private boolean fullyHighlighted;
	private String[] matchedWords;

}
