package com.test.apply.infrastructure.adapter.rest.controllers;

import com.test.apply.request.FilterRequest;
import com.test.apply.response.Response;
import com.test.apply.response.ResponseServiceAlgolia;
import com.test.apply.tasks.Task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	@Autowired
	private Task task;
	
	@PostMapping("/filter")
	public Response getDataFilter(@RequestBody FilterRequest request) {
		return new Response();
	}

	@GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseServiceAlgolia testService() {
		return task.getDataApiHour();
	}

}
