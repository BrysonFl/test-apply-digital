package com.test.apply.entity;

import lombok.Data;

@Data
public class ProcessingTimingsMS {
	
	private AfterFetch afterFetch;
	private Integer total;

}
